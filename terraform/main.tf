resource "aws_s3_bucket" "weather_app_bucket" {
  bucket = "weather-app-bucket-2b50e5ff15bef5dc0a68"

  lifecycle {
    prevent_destroy = false
  }
}

resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "ecsTaskExecutionRole1"
  assume_role_policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "ecs-tasks.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  }
  EOF
}

resource "aws_iam_role" "ecsTaskRunRole" {
  name               = "ecsTaskRunRole"
  assume_role_policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": [
            "ecs-tasks.amazonaws.com"
          ]
        },
        "Effect": "Allow",
        "Sid": ""
      }
    ]
  }
  EOF
}

resource "aws_s3_bucket_policy" "allow_acccess_from_ecs" {
  bucket = aws_s3_bucket.weather_app_bucket.id
  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "AWS": "${aws_iam_role.ecsTaskRunRole.arn}"
        },
        "Action": "s3:*",
        "Resource": [
          "arn:aws:s3:::${aws_s3_bucket.weather_app_bucket.bucket}/*",
          "arn:aws:s3:::${aws_s3_bucket.weather_app_bucket.bucket}"
        ]
      }
    ]
  }
  EOF
}

resource "aws_default_vpc" "default_vpc" {
}

resource "aws_default_subnet" "default_subnet" {
  availability_zone = "eu-west-1a"
}

resource "aws_ecs_cluster" "app_ecs_cluster" {
  name = "app-cluster"
}

resource "aws_ecs_service" "weather_app_ecs_service" {
  name               = "weather-app-service"
  cluster            = aws_ecs_cluster.app_ecs_cluster.id
  task_definition    = aws_ecs_task_definition.weather_app_ecs_task.arn
  launch_type        = "FARGATE"
  network_configuration {
    subnets          = [aws_default_subnet.default_subnet.id]
    assign_public_ip = true
  }
  desired_count = 1
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = "${aws_iam_role.ecsTaskExecutionRole.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_ecs_task_definition" "weather_app_ecs_task" {
  family                   = "weather-app-task"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  memory                   = "1024"
  cpu                      = "512"
  task_role_arn            = aws_iam_role.ecsTaskRunRole.arn
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn
  container_definitions    = jsonencode([
    {
      name      = "weather-app"
      image     = "${aws_ecr_repository.weather_app_repo.repository_url}:latest"
      essential = true
      cpu       = 256
      memory    = 512
      portMappings = [
        { 
          containerPort = 80
          hostPort      = 80
        }
      ]
      environment = [
        {
          name          = "S3_BUCKET"
          value         = "${aws_s3_bucket.weather_app_bucket.bucket}"
        },
        {
          name          = "FILE"
          value         = "weather_data.json"
        }
      ]
      healthCheck = {
        retries = 5
        command = ["CMD-SHELL", "curl -f http://localhost:80/health || exit 1"]
        timeout = 15
        interval = 30
        startPeriod = 30
      }
    },
    {
      name      = "weather-app-updater"
      image     = "${aws_ecr_repository.weather_app_updater_repo.repository_url}:latest"
      essential = true
      cpu       = 256
      memory    = 512
      environment = [
        {
          name          = "S3_BUCKET"
          value         = "${aws_s3_bucket.weather_app_bucket.bucket}"
        },
        {
          name          = "FILE"
          value         = "weather_data.json"
        },
        {
          name          = "LOCATION_LAT"
          value         = "50.09547291341754"
        },
        {
          name          = "LOCATION_LON"
          value         = "14.45262604894543"
        },
        {
          name          = "API_KEY"
          value         = "ef63cf23de76ee9dec101312d295298e"
        },
      ]
    }
  ])
}
