resource "aws_ecr_repository" "weather_app_repo" {
  name                 = "weather-app-repo"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository_policy" "weather_app_repo_policy" {
  repository = aws_ecr_repository.weather_app_repo.name
  policy     = <<EOF
  {
    "Version": "2008-10-17",
    "Statement": [
      {
        "Sid": "full ecr access",
        "Effect": "Allow",
        "Principal": {
          "Service": [],
          "AWS": [
            "${aws_iam_role.ecsTaskExecutionRole.arn}"
          ]
        },
        "Action": [
          "ecr:*"
        ]
      }
    ]
  }
  EOF
}

resource "aws_ecr_lifecycle_policy" "weather_app_repo_remove_untagged" {
  repository = aws_ecr_repository.weather_app_repo.name

  policy = <<EOF
  {
    "rules": [
      {
        "rulePriority": 1,
        "description": "Expire images older than 14 days",
        "selection": {
          "tagStatus": "untagged",
          "countType": "sinceImagePushed",
          "countUnit": "days",
          "countNumber": 14
        },
        "action": {
          "type": "expire"
        }
      }
    ]
  }
  EOF
}

resource "aws_ecr_repository" "weather_app_updater_repo" {
  name                 = "weather-app-updater-repo"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository_policy" "weather_app_updater_repo_policy" {
  repository = aws_ecr_repository.weather_app_updater_repo.name
  policy     = <<EOF
  {
    "Version": "2008-10-17",
    "Statement": [
      {
        "Sid": "full ecr access",
        "Effect": "Allow",
        "Principal": {
          "Service": [],
          "AWS": [
            "${aws_iam_role.ecsTaskExecutionRole.arn}"
          ]
        },
        "Action": [
          "ecr:*"
        ]
      }
    ]
  }
  EOF
}

resource "aws_ecr_lifecycle_policy" "weather_app_updater_repo_remove_untagged" {
  repository = aws_ecr_repository.weather_app_updater_repo.name

  policy = <<EOF
  {
    "rules": [
      {
        "rulePriority": 1,
        "description": "Expire images older than 14 days",
        "selection": {
          "tagStatus": "untagged",
          "countType": "sinceImagePushed",
          "countUnit": "days",
          "countNumber": 14
        },
        "action": {
          "type": "expire"
        }
      }
    ]
  }
  EOF
}