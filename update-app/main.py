import urllib.request
import boto3
import os
import time

def download(lat, lon, api_key):
    try:
        url = "https://api.openweathermap.org/data/2.5/weather?units=metric&lat=%s&lon=%s&appid=%s" % (lat, lon, api_key)
        s = urllib.request.urlopen(url).read().decode()
        return s
    except:
        return False

def saveToS3(bucket, file, data):
    try:
        session = boto3.Session()
        s3 = session.resource('s3')

        object = s3.Object(bucket, file)
        txt_data = data
        result = object.put(Body=txt_data)

        res = result.get('ResponseMetadata')

        if res.get('HTTPStatusCode') == 200:
            return True
        else:
            return False
    except:
        return False

if __name__ == "__main__":
    lat = os.getenv('LOCATION_LAT')
    lon = os.getenv('LOCATION_LON')
    api_key = os.getenv('API_KEY')

    bucket = os.getenv('S3_BUCKET')
    file = os.getenv('FILE')

    update_interval = os.getenv('UPDATE_INTERVAL', 900) # Default 15 minutes

    while True:
        data = download(lat, lon, api_key)

        if data:
            if saveToS3(bucket, file, data):
                print("OK - Data updated")
            else:
                print("Data not saved")
        else:
            print("Data not downloaded")
        
        print("Sleep for %s secons, good night" % update_interval)
        time.sleep(update_interval)

