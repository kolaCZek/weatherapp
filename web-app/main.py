from http.server import BaseHTTPRequestHandler, HTTPServer
from io import BytesIO
import boto3
import json
import os

serverPort = 80

class MyServer(BaseHTTPRequestHandler):
    def getS3Data(self):
        try:
            s3_bucket = os.getenv('S3_BUCKET')
            s3_file = os.getenv('FILE')

            session = boto3.Session()
            s3_client = session.client("s3")

            f = BytesIO()
            s3_client.download_fileobj(s3_bucket, s3_file, f)
            dta = json.loads(f.getvalue())
        except Exception as ex:
            print(ex)
            return "Whops... %s" % ex
 
        try:
            txt = "<p>Weather for %s (Lat: %s Lon: %s)</p>" % (dta['name'], dta['coord']['lat'], dta['coord']['lon'])
            txt += "<table><tr><th>temp</th><td>%s &deg;C</td></tr>" % dta['main']['temp']
            txt += "<tr><th>temp min</th><td>%s &deg;C</td></tr>" % dta['main']['temp_min']
            txt += "<tr><th>temp max</th><td>%s &deg;C</td></tr>" % dta['main']['temp_max']
            txt += "<tr><th>humidity</th><td>%s &percnt;</td></tr>" % dta['main']['humidity']
            txt += "<tr><th>pressure</th><td>%s hPa</td></tr>" % dta['main']['pressure']
            txt += "<tr><th>wind speed</th><td>%s m/s</td></tr>" % dta['wind']['speed']
            txt += "<tr><th>wind dir.</th><td>%s &deg;</td></tr></table>" % dta['wind']['deg']
        except:
            txt = "Can not format response :("

        return txt

    def do_GET(self):
        if self.path == "/health":
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes("OK", "utf-8"))

        elif self.path == "/":
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes("<html><head><title>Simple Weather App</title></head><body>", "utf-8"))
            self.wfile.write(bytes("<p>%s</p>" % self.getS3Data(), "utf-8"))
            self.wfile.write(bytes("</body></html>", "utf-8"))
        else:
            self.send_response(404)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes("<html><head><title>Simple Weather App - 404 Not found</title></head><body>", "utf-8"))
            self.wfile.write(bytes("<p>Not found...</p>", "utf-8"))
            self.wfile.write(bytes("</body></html>", "utf-8"))
            
if __name__ == "__main__":        
    webServer = HTTPServer(("", serverPort), MyServer)
    print("Server started on port %s" % (serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")

