import sys
import json

def get_updated_task_defn(task_defn_file_name, updated_image_web, updated_image_uploader):
    with open(task_defn_file_name, 'r') as f:
        data = json.load(f)
        if 'weather-app-repo' in data["containerDefinitions"][0]["image"]:
            data["containerDefinitions"][0]["image"] = updated_image_web
        else:
            data["containerDefinitions"][0]["image"] = updated_image_uploader
        if 'weather-app-updater-repo' in data["containerDefinitions"][1]["image"]:
            data["containerDefinitions"][1]["image"] = updated_image_uploader
        else:
            data["containerDefinitions"][1]["image"] = updated_image_web
        del data["status"]
        del data["registeredAt"]
        del data["registeredBy"]
        del data["requiresAttributes"]
        del data["compatibilities"]
        del data["taskDefinitionArn"]
        del data["revision"]
    return data


def update_task_defn(task_defn_file_name, data):
    with open(task_defn_file_name, 'w') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

args = sys.argv[1:]
task_defn_file_name = args[0]
updated_image_web = args[1]
updated_image_uploader = args[2]

updated_task_defn = get_updated_task_defn(task_defn_file_name, updated_image_web, updated_image_uploader)
update_task_defn(task_defn_file_name, updated_task_defn)