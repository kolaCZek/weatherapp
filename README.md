# Weather App

Simple application that downloads current weather and displays it as simple web page.

## Prerequisites

* The [Terraform CLI](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) installed.
* The [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) installed.
* [AWS account](https://aws.amazon.com/free) and [associated credentials](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html) that allow you to create resources.

## Installation

Use terraform placed in [./terraform](terraform) to build AWS infrastracture.

Terraform will create ECR repositories, deploy ECS cluster (Fargate) and service with two containers.

However, there is a little design flaw. Terraform wants to create ECS task before the image is pushed to a newly created repository. Service deploy will fail. The first application deployment workaround is to create repositories, push the images and then create the rest of the infrastructure.

```bash
cd ./terraform
# Initialize terraform
terraform init
# Create repositories
terraform apply --target aws_ecr_repository.weather_app_repo --target aws_ecr_repository.weather_app_updater_repo
# Push first version of app with tag :latest (can be dummy container)
docker push <weather_app_repo>:latest
docker push <weather_app_updater_repo>:latest
# Provision the rest of infrastructure
terraform apply
```

## How it works

The application consists of two Docker containers.

The [weather-app](web-app) container fetches current json data from S3 and manages the web interface of the application. The [weather-app-updater](update-app) container handles periodic data retrieval from the [OpenWeather API](https://openweathermap.org/current) and updates the data in S3 Bucket. GPS coordinates, ApiKey and update period can be parameterized through ENV variables.

![Application schema](doc/schema.png "Schema")

## TODO

* Fix the chicken and a egg situation with repository and docker images on the first terraform run.
* Add CloudFront to have a cool domain, caching, SSL, etc.
* weather-app-updater - Check if the response from the datasource is valid and contains relevant data.
* Pipeline - Create and deploy new docker image only if there is a change in the application code.
* Include Terraform to the GitLab pipeline.
* Monitoring & Observability - The current version of the app is completely unmonitored.